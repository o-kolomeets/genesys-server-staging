/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.genesys2.util.MCPDUtil;
import org.junit.Test;

public class MCPDUtilTest {

	@Test
	public void testMcpdArray() {

		assertThat("Result should be null", MCPDUtil.toMcpdArray(null), is(nullValue()));
		
		List<Integer> ints = new ArrayList<Integer>();
		assertThat("Result should be null", MCPDUtil.toMcpdArray(ints), is(nullValue()));
		
		ints.add(1);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("1"));
		
		ints.add(2);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("1;2"));
		
		ints.add(3);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("1;2;3"));
		
		ints.remove(0);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("2;3"));

		ints.add(null);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("2;3"));
		
		ints.add(4);
		assertThat("Result incorrect", MCPDUtil.toMcpdArray(ints), equalTo("2;3;4"));
	}

	@Test
	public void testToStrings() {
		assertThat("List should be null", MCPDUtil.toList(null), is(nullValue()));
		
		assertThat("List should have 1 element", MCPDUtil.toList("NOR051;"), hasSize(equalTo(1)));
		assertThat("Missing element", MCPDUtil.toList("NOR051;"), contains("NOR051"));

		assertThat("List should have 1 element", MCPDUtil.toList(";NOR051;"), hasSize(equalTo(1)));
		assertThat("Missing element", MCPDUtil.toList(";NOR051;"), contains("NOR051"));
		
		assertThat("List should have 1 element", MCPDUtil.toList("NOR051;;"), hasSize(equalTo(1)));
		assertThat("Missing element", MCPDUtil.toList("NOR051;;"), contains("NOR051"));

		assertThat("List should have 2 elements", MCPDUtil.toList("AUS053;NOR051"), hasSize(equalTo(2)));
		assertThat("Missing element", MCPDUtil.toList("AUS053;NOR051"), contains("AUS053", "NOR051"));

		assertThat("List should have 2 elements", MCPDUtil.toList("; ; AUS053;NOR051"), hasSize(equalTo(2)));
		assertThat("Missing element", MCPDUtil.toList("; ; AUS053;NOR051"), contains("AUS053", "NOR051"));
	}

}
