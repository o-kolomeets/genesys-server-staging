/*
 * Copyright 2016 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.service.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.Collections;
import java.util.UUID;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionHistoric;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Ensure that deleted accessions end up as {@link AccessionHistoric} entities.
 */
@Transactional
public class AccessionHistoryTest extends GenesysServicesTest {
	private static final String ACCENUMB = "ACC-001";
	private static final String INSTCODE = "INS001";

	private FaoInstitute faoInstitute;
	private Country country;
	private Taxonomy2 taxonomy2;
	private Accession accession;

	@Before
	public void setup() throws InterruptedException {
		faoInstitute = new FaoInstitute();
		faoInstitute.setFullName("This is name of institute");
		faoInstitute.setCurrent(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setMaintainsCollection(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setAccessionCount(1);
		faoInstitute.setUniqueAcceNumbs(true);
		faoInstitute.setCode(INSTCODE);

		instituteService.update(Collections.singletonList(faoInstitute));

		taxonomy2 = taxonomyService.internalEnsure("Genus", "species", "", "", "");

		country = new Country();
		country.setCode3("UKR");
		country.setName("Ukraine");
		countryRepository.save(country);

		accession = new Accession();
		accession.setAccessionId(new AccessionId());
	}

	@After
	public void teardown() {
		accessionHistoricRepository.deleteAll();
		accessionRepository.deleteAll();
		countryRepository.deleteAll();
		instituteRepository.deleteAll();
		taxonomyRepository.deleteAll();
	}

	@Test
	public void testDeleteAccession1() throws NonUniqueAccessionException {
		accession.setAccessionName(ACCENUMB);
		accession.setInstitute(faoInstitute);
		accession.setTaxonomy(taxonomy2);
		accession.setCountryOfOrigin(country);
		accession.getStoRage().add(10);
		accession.getStoRage().add(20);

		genesysService.saveAccessions(faoInstitute, Collections.singletonList(accession));

		accession = genesysService.getAccession(INSTCODE, ACCENUMB);
		assertThat("INSTCODE must match", accession.getInstituteCode(), is(INSTCODE));
		assertThat("Institute#code must match", accession.getInstitute().getCode(), is(INSTCODE));
		assertThat("ACCENUMB must match", accession.getAccessionName(), is(ACCENUMB));
		assertThat("stoRage must match", accession.getStoRage(), contains(10, 20));
		UUID uuid = accession.getUuid();

		// Delete accession
		genesysService.removeAccessions(faoInstitute, Collections.singletonList(accession));
		Accession a = genesysService.getAccession(INSTCODE, ACCENUMB);
		assertThat(a, nullValue());

		AccessionHistoric historic = genesysService.getHistoricAccession(uuid);
		assertThat(historic, not(nullValue()));
		assertThat("INSTCODE must match", historic.getInstituteCode(), is(INSTCODE));
		assertThat("Institute#code must match", historic.getInstitute().getCode(), is(INSTCODE));
		assertThat("ACCENUMB must match", historic.getAccessionName(), is(ACCENUMB));
		assertThat("stoRage must match", historic.getStoRage(), contains(10, 20));
	}

}
