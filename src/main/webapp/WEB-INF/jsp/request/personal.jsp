<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="request.page.title" />
	</h1>

	<cms:blurb blurb="${blurp}" />
	
    <gui:alert type="danger" display="${error ne null}">
		<c:out value="${error.message}" />
    </gui:alert>

    <gui:alert type="warning" display="${smta ne null and smta ne true}">
        <spring:message code="request.smta-not-accepted"/>
    </gui:alert>
	
	<form method="post" action="<c:url value="/request/submit" />" class="form-horizontal">
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.your-email" /></label>
			<div class="col-lg-9">
				<input type="text" name="email" class="span3 required email form-control" value="${requestEmail}" />
			</div>
		</div>

		<security:authorize access="isAnonymous()">
			<div class="form-group">
				<label class="col-lg-3 control-label"><spring:message code="captcha.text" /></label>
				<div class="col-lg-9">
					<local:captcha siteKey="${captchaSiteKey}" />
				</div>
			</div>
		</security:authorize>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.accept-smta" /></label>
			<div class="col-lg-9">
				<label class="col-xs-12"><input type="radio" name="smta" value="true"> <spring:message code="request.smta-will-accept" /></label>
				<label class="col-xs-12"><input type="radio" name="smta" value="false"> <spring:message code="request.smta-will-not-accept" /></label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.purpose" /></label>
			<div class="col-lg-9">
				<label class="col-xs-12"><input type="radio" name="purpose" value="1" checked> <spring:message code="request.purpose.1" /></label>
				<label class="col-xs-12"><input type="radio" name="purpose" value="0"> <spring:message code="request.purpose.0" /></label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-3 control-label"><spring:message code="request.notes" /></label>
			<div class="col-lg-9">
				<textarea name="notes" class="form-control"><c:out value="${notes}" /></textarea>
			</div>
		</div>
		
		<div class="form-actions">
			<input class="btn btn-primary" type="submit" value="<spring:message code="request.start-request" />" />
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

</body>
</html>