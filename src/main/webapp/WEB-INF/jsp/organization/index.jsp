<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="organization.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="organization.page.list.title" />
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<a href="<c:url value="/org/ORGANIZATION/edit" />" class="close"> <spring:message code="add" />
			</a>
		</security:authorize>
	</h1>



	<div class="nav-header">
		<spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" />
		<br />
		<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
		<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
	</div>

	<ul class="funny-list">
		<c:forEach items="${pagedData.content}" var="organization" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show pull-left" href="<c:url value="/org/${organization.slug}" />"><b><c:out value="${organization.slug}" /></b> <c:out value="${organization.title}" /></a></li>
		</c:forEach>
	</ul>

</body>
</html>