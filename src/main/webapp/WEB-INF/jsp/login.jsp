<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="page.login"/></title>
</head>
<body>
	<h1 class="green-bg"><spring:message code="page.login" /></h1>
	
	<!-- Alerts? -->
	<gui:alert type="danger" display="${param['error'] ne null}">
		<spring:message code="login.invalid-credentials" />
	</gui:alert>
	<gui:alert type="danger" display="${error ne null}">
		<spring:message code="login.invalid-token" />
	</gui:alert>

	<form role="form" method="POST" action="<c:url value="/login-attempt" />" class="form-horizontal">
        <div class="form-group">
            <label for="j_username" class="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label"><spring:message code="login.username"/></label>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <input type="text" id="j_username" name="username" class="form-control grabfocus" autofocus="autofocus" />
            </div>
        </div>

        <div class="form-group">
            <label for="j_password"  class="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label"><spring:message code="login.password"/></label>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <input type="password" id="j_password" name="password" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12 checkbox">
            <label class="control-label" for='_spring_security_remember_me'>
                <input type='checkbox' name='_spring_security_remember_me'  id='_spring_security_remember_me'/>
                <spring:message code="login.remember-me"/>
            </label>
            </div>
        </div>
        <div class="form-group transparent">
            <div class="col-lg-offset-2 col-lg-10 col-md-offset-3 col-md-9 col-sm-offset-3 col-sm-9 col-xs-12">
	            <input type="submit" value="<spring:message code="login.login-button" />" class="btn btn-primary" />
	            <a href="<c:url value="/google/login" />" class="btn btn-default google-signin"><spring:message code="login.with-google-plus"/></a>
	            <a href="registration" id="registration" class="btn btn-default"><spring:message code="login.register-now"/></a>
	           	<a href="/profile/forgot-password" id="forgot-password" class="btn btn-default"><spring:message code="login.forgot-password"/></a>
        	</div>
        </div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</body>
</html>