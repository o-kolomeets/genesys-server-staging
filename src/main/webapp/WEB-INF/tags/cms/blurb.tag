<%@ tag description="Display a text blurb" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>
<%@ attribute name="blurb" required="true" type="org.genesys2.server.model.impl.Article" %>

<c:if test="${blurb ne null}">
	<div class="free-text blurp" dir="${blurb.lang=='fa' || blurb.lang=='ar' ? 'rtl' : 'ltr'}">
		<c:out value="${blurb.body}" escapeXml="false" />
	</div>
	
	<local:not-translated display="${blurb.lang != pageContext.response.locale.language}" />
</c:if>