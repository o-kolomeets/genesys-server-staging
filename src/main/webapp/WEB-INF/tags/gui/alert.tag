<%@ tag description="Alert display" pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="type" required="true" description="Alert style: success, info, warning, danger" type="java.lang.String" %>
<%@ attribute name="display" required="false" description="Should the alert be displayed or not" rtexprvalue="true" type="java.lang.Boolean" %>

<c:if test="${(empty display) or display eq true}">
	<div class="alert alert-<c:out value="${type}" />">
		<jsp:doBody />
	</div>
</c:if>
