<%@ tag description="Display filter group" pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="termResult" required="true" type="org.springframework.data.elasticsearch.core.facet.result.TermResult" %>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<%@ attribute name="messageCode" required="true" type="java.lang.String"%>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-section">
    <h4 class="section-heading">
        <spring:message code="${messageCode}"/>
    </h4>
    <div class="section-inner-content clearfix">
        <local:term-result termResult="${termResult}" count="${count}" type="${type}" />
    </div>
</div>



<%--<div class="col-xs-12 col-sm-6 row-section">--%>
    <%--<h4 id="stats-instcode" class="section-heading"><spring:message code="${messageCode}"/></h4>--%>
    <%--<local:term-result termResult="${termResult}" count="${count}" type="${type}" />--%>
<%--</div>--%>





