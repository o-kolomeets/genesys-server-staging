/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

/*
 * Looks like {@link AccessionTrait}
 */
public class ExperimentAccessionTrait {
	private long experimentId;
	private long accessionId;
	private Object value;

	public ExperimentAccessionTrait(final long experimentId, final long accessionId, final Object value) {
		this.experimentId = experimentId;
		this.accessionId = accessionId;
		this.value = value;
	}

	public long getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(final long experimentId) {
		this.experimentId = experimentId;
	}

	public long getAccessionId() {
		return accessionId;
	}

	public void setAccessionId(long accessionId) {
		this.accessionId = accessionId;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(final Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "{" + value + ", " + experimentId + "}";
	}
}
