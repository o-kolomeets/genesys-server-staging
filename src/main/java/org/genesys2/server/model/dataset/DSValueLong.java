package org.genesys2.server.model.dataset;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class DSValueLong extends DSValue<Long> {

	private Long vall;

	@Override
	public Long getValue() {
		return vall;
	}

	@Override
	public void setValue(Long value) {
		this.vall = value;
	}
}
