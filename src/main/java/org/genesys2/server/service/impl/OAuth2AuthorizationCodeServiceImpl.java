/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.oauth.OAuthCode;
import org.genesys2.server.persistence.domain.OAuthCodePersistence;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.OAuth2AuthorizationCodeCleanup;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.DefaultAuthorizationRequest;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.AuthorizationRequestHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("verificationCodeService")
@Transactional
public class OAuth2AuthorizationCodeServiceImpl implements OAuth2AuthorizationCodeCleanup, AuthorizationCodeServices {
	private static final Log LOG = LogFactory.getLog(OAuth2AuthorizationCodeServiceImpl.class);

	private final RandomValueStringGenerator generator = new RandomValueStringGenerator();

	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private OAuthCodePersistence oauthCodeRepository;

	@Autowired
	private UserService userService;

	/**
	 * Cleanup executed every 10 minutes
	 */
	@Override
	@Scheduled(fixedDelay = 600000)
	public void removeExpired() {
		final Date olderThan = new Date(new Date().getTime() - 600000);
		LOG.debug("Removing OAuth verification codes from before: " + olderThan);
		final int count = oauthCodeRepository.deleteOlderThan(olderThan);
		if (count > 0)
			LOG.info("Removed expired OAuth verification codes: " + count);
	}

	@Override
	public String createAuthorizationCode(AuthorizationRequestHolder authentication) {
		for (int i = 5; i >= 0; i--) {
			final String code = generator.generate();
			try {
				store(code, authentication);
				return code;
			} catch (final Throwable e) {
				LOG.warn(e);
			}
		}
		throw new RuntimeException("Failed to generate a unique verification code.");
	}

	@Override
	public AuthorizationRequestHolder consumeAuthorizationCode(String code) throws InvalidGrantException {
		try {
			final AuthorizationRequestHolder auth = this.remove(code);
			if (auth == null) {
				throw new InvalidGrantException("Invalid authorization code: " + code);
			}
			return auth;
		} catch (IOException e) {
			throw new InvalidGrantException(e.getMessage(), e);
		}
	}

	protected void store(String code, AuthorizationRequestHolder authentication) {
		LOG.debug("store code=" + code + " arh=" + authentication);

		final OAuthCode oauthCode = new OAuthCode();
		oauthCode.setCode(code);

		LOG.debug("authentication.userAuthentication " + authentication.getUserAuthentication().getClass());
		Object principal = authentication.getUserAuthentication().getPrincipal();

		if (principal != null && principal instanceof AuthUserDetails) {
			LOG.debug("principal=" + principal + " " + principal.getClass());
			AuthUserDetails userDetails = (AuthUserDetails) principal;
			oauthCode.setUserUuid(userDetails.getUsername());
		}

		AuthorizationRequest ar = authentication.getAuthenticationRequest();

		String clientId = ar.getClientId();
		LOG.debug("clientId=" + clientId);

		oauthCode.setClientId(ar.getClientId());
		oauthCode.setRedirectUri(ar.getRedirectUri());
		try {
			oauthCode.setScopes(mapper.writeValueAsString(ar.getScope()));
		} catch (JsonProcessingException e) {
			LOG.error("Error serializing oauthCode.scopes", e);
		}

		LOG.debug("redirectUri=" + ar.getRedirectUri() + " state=" + ar.getState());

		// User's granted authorities, should be obtained from user
		for (GrantedAuthority x : ar.getAuthorities()) {
			LOG.debug("grantedAuth=" + x);
		}

		// Note: contains remaining HTTP request parameters, not useful
		for (String x : ar.getApprovalParameters().keySet()) {
			LOG.debug("approvalParameters.key=" + x + " val=" + ar.getApprovalParameters().get(x));
		}

		// Note: contains OAuth authorization parameters, nothing really useful
		for (String x : ar.getAuthorizationParameters().keySet()) {
			LOG.debug("authorizationParameters.key=" + x + " val=" + ar.getAuthorizationParameters().get(x));
		}

		// Note: was blank
		for (String x : ar.getResourceIds()) {
			LOG.debug("resourceId=" + x);
		}

		// Note: contains requested OAuth response type ("code" in our case)
		for (String x : ar.getResponseTypes()) {
			LOG.debug("responseType=" + x);
		}

		oauthCodeRepository.save(oauthCode);
	}

	protected AuthorizationRequestHolder remove(String code) throws IOException {
		final OAuthCode oauthCode = oauthCodeRepository.findByCode(code);
		if (oauthCode != null) {
			oauthCodeRepository.delete(oauthCode);

			@SuppressWarnings("unchecked")
			DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(oauthCode.getClientId(), mapper.readValue(oauthCode.getScopes(),
					HashSet.class));

			authorizationRequest.setRedirectUri(oauthCode.getRedirectUri());
			authorizationRequest.setApproved(true);

			PreAuthenticatedAuthenticationToken userAuthentication = null;
			if (oauthCode.getUserUuid() != null) {
				UserDetails userDetails = userService.getUserDetails(oauthCode.getUserUuid());

				userAuthentication = new PreAuthenticatedAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				userAuthentication.setAuthenticated(true);
			}
			AuthorizationRequestHolder arh = new AuthorizationRequestHolder(authorizationRequest, userAuthentication);
			return arh;
		} else {
			return null;
		}
	}

}
