/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.List;

import javax.xml.bind.ValidationException;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;

import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.TraitService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/crops", "/json/v0/crops" })
public class CropsController extends RestController {

	@Autowired
	GenesysService genesysService;

	@Autowired
	TraitService traitService;

	@Autowired
	CropService cropService;

	/**
	 * List all crops
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object listCrops() {
		LOG.info("Listing crops");
		final List<Crop> crops = cropService.list(LocaleContextHolder.getLocale());
		return crops;
	}

	/**
	 * Add a crop
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object createCrop(@RequestBody Crop cropJson) throws ValidationException {
		LOG.info("Creating crop");
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(cropJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Crop does not validate", violations);
		}

		Crop crop = cropService.getCrop(cropJson.getShortName());

		if (crop == null) {
			crop = cropService.addCrop(cropJson.getShortName(), cropJson.getName(), cropJson.getDescription(), cropJson.getI18n());
		} else {
			crop = cropService.updateCrop(crop, cropJson.getName(), cropJson.getDescription(), cropJson.getI18n());
		}

		return crop;
	}

	/**
	 * Get crop details /crops/{shortName}
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object getCrop(@PathVariable("shortName") String shortName) throws AuthorizationException {
		LOG.info("Getting crop " + shortName);
		return cropService.getCrop(shortName);
	}

	/**
	 * Delete crop /crops/{shortName}
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Crop deleteCrop(@PathVariable("shortName") String shortName) throws AuthorizationException {
		LOG.info("Getting crop " + shortName);
		return cropService.delete(cropService.getCrop(shortName));
	}

	/**
	 * Get crop descriptors /crops/{shortName}/descriptors
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/descriptors", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object getCropDescriptors(@PathVariable("shortName") String shortName) throws AuthorizationException {
		LOG.info("Getting crop descriptors " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		final Page<Parameter> descriptors = traitService.listTraits(crop, new PageRequest(0, 50));
		return OAuth2Cleanup.clean(descriptors);
	}

	/**
	 * Get crop taxonomy rules /crops/{shortName}/rules
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/rules", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object getCropRules(@PathVariable("shortName") String shortName) throws AuthorizationException {
		LOG.info("Getting crop rules " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		final List<CropRule> cropRules = cropService.getCropRules(crop);
		return cropRules;
	}

	/**
	 * Get crop taxonomy rules /crops/{shortName}/rules
	 * 
	 * @return
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/rules", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<CropRule> updateCropRules(@PathVariable("shortName") String shortName, @RequestBody List<CropRule> rules) throws AuthorizationException {
		LOG.info("Updating crop rules for " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null)
			throw new ResourceNotFoundException("No crop " + shortName);

		cropService.setCropRules(crop, rules);

		return cropService.getCropRules(crop);
	}

	/**
	 * Get crop taxonomies /crops/{shortName}/taxa
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/{shortName}/taxa", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Object getCropTaxa(@PathVariable("shortName") String shortName) throws AuthorizationException {
		LOG.info("Getting crop taxa " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		final Page<CropTaxonomy> cropTaxa = cropService.getCropTaxonomies(crop, new PageRequest(0, 50));
		return OAuth2Cleanup.clean(cropTaxa);
	}

	/**
	 * Rebuild taxonomy-crop lists
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/rebuild", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String rebuild() {
		cropService.rebuildTaxonomies();
		return JSON_OK;
	}

	/**
	 * Rebuild taxonomy-crop lists
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "{shortName}/rebuild", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String rebuildCrop(@PathVariable("shortName") String shortName) {
		LOG.info("Updating crop rules for " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null)
			throw new ResourceNotFoundException("No crop " + shortName);

		cropService.rebuildTaxonomies(crop);
		return JSON_OK;
	}

}
