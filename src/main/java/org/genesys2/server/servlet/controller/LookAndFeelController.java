/*
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.servlet.controller;

import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller to render the Look-and-Feel pages for GUI designers
 */
@Controller
public class LookAndFeelController extends BaseController {

	@Autowired
	private CropService cropService;

	@Autowired
	private ContentService contentService;

	@Value("${captcha.siteKey}")
	private String captchaSiteKey;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@RequestMapping("/look-and-feel")
	public String index(ModelMap model) {
		model.addAttribute("cropList", cropService.list(getLocale()));
		model.addAttribute("lastNews", contentService.lastNews());
		model.addAttribute("welcomeBlurp", contentService.getGlobalArticle("about", getLocale()));
		model.addAttribute("captchaSiteKey", captchaSiteKey);

		return "/lookandfeel";
	}
}
