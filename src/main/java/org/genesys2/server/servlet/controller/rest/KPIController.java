/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;

import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.kpi.BooleanDimension;
import org.genesys2.server.model.kpi.Dimension;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionDimension;
import org.genesys2.server.model.kpi.KPIParameter;
import org.genesys2.server.model.kpi.Observation;
import org.genesys2.server.service.KPIService;
import org.genesys2.server.servlet.controller.rest.model.ExecutionDimensionJson;
import org.genesys2.server.servlet.controller.rest.model.ExecutionJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/kpi", "/json/v0/kpi" })
public class KPIController extends RestController {

	@Autowired
	private KPIService kpiService;

	/**
	 * List parameters
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/parameter/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Map<String, String> listParameters() {
		LOG.info("Listing KPI parameters");
		HashMap<String, String> m = new HashMap<>();
		for (KPIParameter kpip : kpiService.listParameters()) {
			m.put(kpip.getName(), kpip.getTitle() + "\n" + kpip.getDescription());
		}
		return m;
	}

	/**
	 * Get parameter
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/parameter/{name:.+}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody KPIParameter getParameter(@PathVariable("name") String name) {
		return kpiService.getParameter(name);
	}

	/**
	 * Delete parameter
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/parameter/{name:.+}", method = { RequestMethod.DELETE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody KPIParameter deleteParameter(@PathVariable("name") String name) {
		return kpiService.delete(kpiService.getParameter(name));
	}

	/**
	 * Update parameter
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/parameter", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody KPIParameter createParameter(@RequestBody KPIParameter parameter) throws ValidationException {
		LOG.info("Updating parameter: " + parameter.getName());
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(parameter);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation failed", violations);
		}

		return kpiService.save(parameter);
	}

	/**
	 * List dimensions
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/dimension/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody HashMap<Long, String> listDimensions() {
		LOG.info("Listing KPI dimensions");
		HashMap<Long, String> m = new HashMap<>();
		for (Dimension<?> dim : kpiService.listDimensions()) {
			m.put(dim.getId(), dim.getName() + " " + dim.getTitle());
		}
		return m;
	}

	/**
	 * Get parameter
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/dimension/{id}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Dimension<?> getDimension(@PathVariable("id") long id) {
		return kpiService.getDimension(id);
	}

	/**
	 * Get parameter
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/dimension/{id}", method = { RequestMethod.DELETE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Dimension<?> deleteDimension(@PathVariable("id") long id) {
		return kpiService.delete(kpiService.getDimension(id));
	}

	/**
	 * Update {@link BooleanDimension}
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/dimension", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Dimension<?> updateBooleanDimension(@RequestBody Dimension<?> dimension) throws ValidationException {
		LOG.info("Updating dimension: " + dimension.getName());
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(dimension);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation failed", violations);
		}

		return kpiService.save(dimension);
	}

	/**
	 * List dimensions
	 * 
	 * @return
	 * @throws AuthorizationException
	 */
	@RequestMapping(value = "/execution/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody HashMap<String, String> listExecution() {
		LOG.info("Listing KPI executions");
		HashMap<String, String> m = new HashMap<>();
		for (Execution exec : kpiService.listExecutions()) {
			m.put(exec.getName(), exec.getTitle());
		}
		return m;
	}

	/**
	 * Get execution
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/execution/{executionName:.+}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ExecutionJson getExecution(@PathVariable("executionName") String executionName) {
		Execution execution = kpiService.getExecution(executionName);
		return ExecutionJson.from(execution);
	}

	/**
	 * Delete execution
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/execution/{executionName:.+}", method = { RequestMethod.DELETE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ExecutionJson deleteExecution(@PathVariable("executionName") String executionName) {
		return ExecutionJson.from(kpiService.delete(kpiService.getExecution(executionName)));
	}

	/**
	 * List observations
	 * 
	 * @return
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/observation/{executionName}/", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Observation> listObservations(@PathVariable("executionName") String executionName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestBody(required = false) Map<String, String> dimensionFilters) {
		return kpiService.listObservations(kpiService.findLastExecutionRun(kpiService.getExecution(executionName)), dimensionFilters, new PageRequest(page - 1,
				50));
	}

	/**
	 * Update {@link BooleanDimension}
	 * 
	 * @return
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/execution/{executionName}/execute", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Observation> execute(@PathVariable("executionName") String executionName) {
		Execution execution = kpiService.getExecution(executionName);
		LOG.info("Running execute on : " + executionName);
		List<Observation> res = kpiService.execute(execution);
		LOG.info("Saving results: count=" + res.size());
		List<Observation> x = kpiService.save(execution, res);
		LOG.info("Done saving results.");
		return x;
	}

	/**
	 * Update {@link BooleanDimension}
	 * 
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/execution", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ExecutionJson updateExecution(@RequestBody ExecutionJson ej) throws ValidationException {
		LOG.info("Updating execution: " + ej.title);
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(ej);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation failed", violations);
		}

		Execution execution = null;
		if (ej.id != null)
			execution = kpiService.getExecution(ej.id);
		if (execution == null)
			execution = new Execution();
		execution.setName(ej.name);
		execution.setTitle(ej.title);
		execution.setParameter(kpiService.getParameter(ej.parameter));
		execution.setVersion(ej.version);

		// Update executionDimensions
		for (int i = execution.getExecutionDimensions().size() - 1; i >= 0; i--) {
			ExecutionDimension ed = execution.getExecutionDimensions().get(i);
			if (!contains(ed, ej.dimensions))
				execution.getExecutionDimensions().remove(i);
		}
		for (ExecutionDimensionJson edj : ej.dimensions) {
			updateOrInsert(edj, execution.getExecutionDimensions());
		}

		return ExecutionJson.from(kpiService.save(execution));
	}

	private void updateOrInsert(ExecutionDimensionJson edj, List<ExecutionDimension> executionDimensions) {
		for (ExecutionDimension ed : executionDimensions) {
			if (ed.getId() != null && ed.getId().equals(edj.id)) {
				ed.setDimension(kpiService.getDimension(edj.dimensionId));
				ed.setField(edj.field);
				ed.setLink(edj.link);
				return;
			}
		}

		ExecutionDimension ed = new ExecutionDimension();
		ed.setDimension(kpiService.getDimension(edj.dimensionId));
		ed.setField(edj.field);
		ed.setLink(edj.link);
		executionDimensions.add(ed);
	}

	private boolean contains(ExecutionDimension ed, Collection<ExecutionDimensionJson> dimensions) {
		for (ExecutionDimensionJson edj : dimensions) {
			if (edj.id != null && edj.id.equals(ed.getId()))
				return true;
		}
		return false;
	}

}
