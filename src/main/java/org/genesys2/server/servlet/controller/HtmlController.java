/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.StatisticsService;
import org.genesys2.server.servlet.filter.LocaleURLFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller which simply handles *.html requests
 */
@Controller
public class HtmlController extends BaseController {

	@Autowired
	private CropService cropService;

	@Autowired
	private ContentService contentService;

	@Value("${captcha.siteKey}")
	private String captchaSiteKey;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private OrganizationService organizationService;

	@RequestMapping("/")
	public String index(HttpServletRequest request) {

		Locale requestLocale = request.getLocale();
		Locale requestedLocale = (Locale) request.getAttribute(LocaleURLFilter.REQUEST_LOCALE_ATTR);
		if (_logger.isDebugEnabled()) {
			_logger.debug("Request locale: " + requestLocale + " requested: " + requestedLocale);
		}

		if (requestedLocale == null && requestLocale != null) {
			_logger.info("Redirecting to request locale: " + requestLocale.getLanguage());
			return "redirect:/" + requestLocale.getLanguage().toLowerCase() + "/welcome";
		}

		return "redirect:/welcome";
	}

	@RequestMapping(value = "welcome")
	public String welcome(ModelMap model) {
		model.addAttribute("cropList", cropService.list(getLocale()));
		model.addAttribute("lastNews", contentService.lastNews());
		model.addAttribute("welcomeBlurp", contentService.getGlobalArticle("welcome", getLocale()));
		model.addAttribute("sideBlurp", contentService.getGlobalArticle("sideBlurp", getLocale()));
		model.addAttribute("organizationsBlurb", contentService.getGlobalArticle("welcome-organizations", getLocale()));

		model.addAttribute("numberOfCountries", statisticsService.numberOfCountries());
		model.addAttribute("numberOfInstitutes", statisticsService.numberOfInstitutes());
		model.addAttribute("numberOfAccessions", statisticsService.numberOfAccessions());
		model.addAttribute("numberOfActiveAccessions", statisticsService.numberOfActiveAccessions());
		model.addAttribute("numberOfHistoricAccessions", statisticsService.numberOfHistoricAccessions());

		model.addAttribute("organizations", organizationService.list(new PageRequest(0, 5)));

		return "/index";
	}

	@RequestMapping(value = "login")
	public String login() {
		return "/login";
	}

	@RequestMapping("/access-denied")
	public void accessDenied() {
		throw new AccessDeniedException("Spring Security denied access to the resource.");
	}

	@RequestMapping(value = "/errors/{code}")
	public String errorHandler(@PathVariable("code") int code) {
		return "/errors/error";
	}
}
