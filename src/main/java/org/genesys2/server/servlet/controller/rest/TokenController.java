/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.genesys2.server.ServiceEndpoints;
import org.genesys2.server.exception.NoSuchTokenException;
import org.genesys2.server.servlet.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0", "/json/v0" })
public class TokenController extends BaseController {

	@Autowired
	private ConsumerTokenServices tokenServices;

	private final PasswordEncoder encoder = new StandardPasswordEncoder();

	@RequestMapping(value = ServiceEndpoints.LIST_USER_TOKENS, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Collection<OAuth2AccessToken> listTokensForUser(@PathVariable String username) throws Exception {
		final OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		checkResourceOwner(username, principal);
		return enhance(tokenServices.findTokensByUserName(username));
	}

	@RequestMapping(value = ServiceEndpoints.REVOKE_USER_TOKEN, method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public SimpleMessage revokeUserToken(@PathVariable String username, @PathVariable String token) throws Exception {
		final OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		checkResourceOwner(username, principal);
		final String tokenValue = getTokenValue(tokenServices.findTokensByUserName(username), token);
		if (tokenValue != null && tokenServices.revokeToken(tokenValue)) {
			return new SimpleMessage("ok", "user token revoked");
		}
		throw new NoSuchTokenException("Token not found");
	}

	@RequestMapping(value = ServiceEndpoints.LIST_CLIENT_TOKEN, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Collection<OAuth2AccessToken> listTokensForClient(@PathVariable String client) throws Exception {
		final OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		checkClient(client, principal);
		return enhance(tokenServices.findTokensByClientId(client));
	}

	@RequestMapping(value = ServiceEndpoints.REVOKE_CLIENT_TOKEN, method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public SimpleMessage revokeClientToken(@PathVariable String client, @PathVariable String token) throws Exception {
		final OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		checkClient(client, principal);
		final String tokenValue = getTokenValue(tokenServices.findTokensByClientId(client), token);
		if (tokenValue != null && tokenServices.revokeToken(tokenValue)) {
			return new SimpleMessage("ok", "client token revoked");
		}
		throw new NoSuchTokenException("Token not found");
	}

	@ExceptionHandler(NoSuchTokenException.class)
	public ResponseEntity<Void> handleNoSuchToken(NoSuchTokenException e) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}

	private String getTokenValue(Collection<OAuth2AccessToken> tokens, String hash) {
		for (final OAuth2AccessToken token : tokens) {
			try {
				return token.getValue();
			} catch (final Exception e) {
				// it doesn't match
			}
		}
		return null;
	}

	private Collection<OAuth2AccessToken> enhance(Collection<OAuth2AccessToken> tokens) {
		final Collection<OAuth2AccessToken> result = new ArrayList<OAuth2AccessToken>();
		for (final OAuth2AccessToken prototype : tokens) {
			final DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(prototype);
			final Map<String, Object> map = new HashMap<String, Object>(token.getAdditionalInformation());
			// The token doesn't have an ID in the token service, but we need
			// one for the endpoint, so add one here
			map.put(token.getTokenType(), encoder.encode(token.getValue()));
			try {
				final String clientId = tokenServices.getClientId(token.getValue());
				if (clientId != null) {
					map.put("client_id", clientId);
				}
			} catch (final InvalidTokenException e) {
				// Ignore defensively in case of bugs in token services
			}
			token.setAdditionalInformation(map);
			result.add(token);
		}
		return result;
	}

	private void checkResourceOwner(String user, Principal principal) {
		if (principal instanceof OAuth2Authentication) {
			final OAuth2Authentication authentication = (OAuth2Authentication) principal;
			if (!authentication.isClientOnly() && !user.equals(principal.getName())) {
				throw new AccessDeniedException(String.format("User '%s' cannot obtain tokens for user '%s'", principal.getName(), user));
			}
		} else if (!user.equals(principal.getName())) {
			throw new AccessDeniedException(String.format("User '%s' cannot obtain tokens for user '%s'", principal.getName(), user));
		}

	}

	private void checkClient(String client, Principal principal) {
		if (principal instanceof OAuth2Authentication) {
			final OAuth2Authentication authentication = (OAuth2Authentication) principal;
			if (!authentication.isClientOnly() || !client.equals(principal.getName()) && !isAdmin(principal)) {
				throw new AccessDeniedException(String.format("Client '%s' cannot obtain tokens for client '%s'", principal.getName(), client));
			}
		}
	}

	private boolean isAdmin(Principal principal) {
		return AuthorityUtils.authorityListToSet(((Authentication) principal).getAuthorities()).contains("USER_ADMIN");
	}

	/**
	 * @param tokenServices
	 *            the consumerTokenServices to set
	 */
	public void setTokenServices(ConsumerTokenServices tokenServices) {
		this.tokenServices = tokenServices;
	}

	public static class SimpleMessage implements Serializable {

		private static final long serialVersionUID = -2090423223347806024L;

		private final String status;

		private final String message;

		public SimpleMessage(String status, String message) {
			this.status = status;
			this.message = message;
		}

		public String getStatus() {
			return status;
		}

		public String getMessage() {
			return message;
		}

		@Override
		public String toString() {
			return "{\"status\"=\"" + status + "\",\"message\"=\"" + message + "\"}";
		}

		@Override
		public int hashCode() {
			return toString().hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof SimpleMessage && toString().equals(obj.toString());
		}

	}
}
