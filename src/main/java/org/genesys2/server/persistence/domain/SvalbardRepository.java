/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.SvalbardDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SvalbardRepository extends JpaRepository<SvalbardDeposit, Long> {

	List<SvalbardDeposit> findByAccession(AccessionId accession);

	@Query("from SvalbardDeposit sd where sd.accession.id in (?1)")
	List<SvalbardDeposit> findAllFor(Collection<Long> accessionIds);

	@Query("delete from SvalbardDeposit sd where sd.id in (?1)")
	@Modifying
	void deleteById(Set<Long> collect);

	/*
	 * Based on: select sd.id, sd.accenumb svacce,a.accenumb acce, sd.genus svgenus, t.genus, sd.instId svinst,
	 * a.instituteId inst from sgsvdeposit sv left join accession a on a.instituteId=sd.instId and
	 * a.accenumb=sd.accenumb inner join taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus limit 10
	 */
	@Query(nativeQuery = true, value = "update sgsvdeposit sd inner join accession a on a.instituteId=sd.instId and a.accenumb=sd.accenumb inner join taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus set sd.acceId=a.id where sd.id in (?1)")
	@Modifying
	int linkDirectly(List<Long> sgsvIds);

	/*
	 * Based on:
	 * 
	 * select sd.*, i.code, ii.code, a.accenumb, t.genus from sgsvdeposit sd inner join faoinstitute i on i.id=sd.instId
	 * inner join accession a on a.accenumb=sd.accenumb inner join faoinstitute ii on ii.codeSGSV=i.code inner join
	 * taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus limit 10;
	 */
	@Query(nativeQuery = true, value = "update sgsvdeposit sd inner join faoinstitute i on i.id=sd.instId inner join accession a on a.accenumb=sd.accenumb inner join faoinstitute ii on ii.codeSGSV=i.code inner join taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus set sd.acceId=a.id where sd.id in (?1)")
	@Modifying
	int linkInDirectly(List<Long> sgsvIds);

	/*
	 * Link accessions to SGSV data by their alternate names.
	 * 
	 * select sd.accenumb svnumb, a.acceNumb, aa.name alias, i.code svinst, aa.usedBy instcode, sd.genus svgenus,
	 * t.genus from sgsvdeposit sd inner join faoinstitute i on i.id=sd.instId inner join accessionalias aa on
	 * aa.name=sd.accenumb and (aa.usedBy=i.code or aa.usedBy=i.codeSGSV) inner join accession a on a.id=aa.accessionId
	 * inner join taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus limit 100;
	 */
	@Query(nativeQuery = true, value = "update sgsvdeposit sd inner join faoinstitute i on i.id=sd.instId inner join accessionalias aa on aa.name=sd.accenumb and (aa.usedBy=i.code or aa.usedBy=i.codeSGSV) inner join accession a on a.id=aa.accessionId inner join taxonomy2 t on t.id=a.taxonomyId2 and t.genus=sd.genus set sd.acceId=a.id where sd.id in (?1)")
	@Modifying
	int linkByAlternativeName(List<Long> sgsvIds);
}
