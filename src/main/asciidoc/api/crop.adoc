[[chApiCrop]]

== Managing Crop data

Genesys maintains a database of crops and crop groups (e.g. forages). In addition to the general
description, each crop defines a list of taxonomic rules that determine which taxonomies are
included (or excluded) in the group.

Crops and crop groups are referred to and identified by the crop's *short name*. The short name
placeholder in documentation below is marked by `{shortName}`. The short name should have no spaces 
and it should contain US-ASCII characters only (a-Z, 0-9).
  

[NOTE]
.Crop Taxonomic rules
=====================================================================
The common crop name of an accession in Genesys is determined by its taxonomy.
=====================================================================


For example, crop https://www.genesys-pgr.org/c/banana[banana] includes accessions with genus _Musa_.


. Each crop and crop group defines its own *taxonomic rules*
. The genus, species and sub-species data of an accession is matched against all taxonomic rules
  .. Accession is linked with all matching crops
  .. Accession may have more than one "crop"


Managing crop data in Genesys is done through methods available in */api/v0/crops*
namespace. 

=== Listing all crops

A number of crops and crop groups are usually defined and used in Genesys. To list all
crops, issue a GET request to /api/v0/crops/ endpoint:

include::{snippets}/crop-list/curl-request.adoc[]

The result is a JSON array of all crops in Genesys:


[source,json,linenums]
----
[{
	"id": 27,
	"version": 1,
	"rdfUri": null,
	"shortName": "apple",
	"name": "Apple",
	"description": null,
	"i18n": "{\"name\":{\"fa\":\"سیب\"}}"
}, {
	"id": 1,
	"shortName": "banana",
	"name": "Banana", ... <1> 
}, {
	"shortName": "barley",
	"name": "Barley", ...
}, 
...]
----
<1> Some JSON elements removed for readability

=== Retrieving crop data

The crop details are retrievable with a HTTP GET method to `/api/v0/crops/{shortName}`. 

[cols="1,3", options="header"]
.URL path parameters
include::{snippets}/crop-get/path-parameters.adoc[]


.Sample `curl` request to fetch maize crop definition
include::{snippets}/crop-get/curl-request.adoc[]

The JSON representation of a single crop record includes the following fields: 

[cols="1,1,2", options="header"]
.Fields of *Crop* records
include::{snippets}/crop-get/response-fields.adoc[]

==== Taxonomic rules

The `/api/v0/crops/{shortName}/rules` endpoint exposes access to crop's taxonomic rules.
We mentioned earlier that accessions with genus _Musa_ belong to bananas, similarly _Zea_
genus belongs to maize.

.Maize taxonomic rules
include::{snippets}/crop-rules-list/curl-request.adoc[]

[source,json,linenums]
----
[{
	"id": 24,
	"included": true, <1>
	"genus": "Zea", <2>
	"species": null, <3>
	"subtaxa": null <4>
}]
----
<1> The combination of genus, species and subtaxa can be either included or explicitly excluded from the crop.
<2> Genus _Zea_ is included in maize when accession's genus is _Zea_.
<3> Species field is `null` meaning that accession species is ignored by this rule.  
<4> Subtaxa field is `null` meaninig that accession subtaxa value is ignored by this rule.

==== Exclusion rule

A rule can explicitly exclude accessions matching a particular combination of genus + species + subtaxa.
This is useful for cases where you wish to include all _Solanum_ species except for selected species 
(e.g. _Solanum melongena_).


=== Registering a new crop

To create a new crop, a JSON with the following data must be submitted:

[source,json,linenums]
----
{
	"shortName": "maize",
	"name": "Maize",
	"description": "Crop description in EN"
}
----

[cols="1,1,2", options="header"]
.Minimum required data to register a crop
include::{snippets}/crop-create/request-fields.adoc[]

The response is a single crop record as stored on the server.


.Example request to register a new crop
include::{snippets}/crop-create/curl-request.adoc[]

=== Localization of crop title and description

The `i18n` field of the JSON crop object is a string encoded JSON object of a two level
JSON formatted dictionary string with first level keys `name` (for the name field)
and `description` (for the description field) and second level keys corresponding to ISO_639_2 
encoded vernacular language tags.  

For example:

[source,json,linenums]
----
{
	"name": {
		"en": "Musa",
		"es": "Musa",
		"ru": "Муса",
		"zh": "穆萨"
	},
	"description": {
		"en": "Bananas and plantains",
		"es": "Los bananos y plátanos",
		"ru": "Бананы и бананы",
		"zh": "香蕉和大蕉"
	}
}
----


=== Updating taxonomic rules

Taxonomic rules can be replaced using one call by providing the new list of rules as the 
body of the HTTP PUT call to `/api/v0/crops/{shortName}/rules`. To specify
that all _Triticum_ and _Aegilops_ species should be included in *wheat* you would 
send the following array of rules to `/api/v0/crops/wheat/rules`: 

[source,json,linenums]
----
[{
	"included": true,
	"genus": "Triticum"
}, {
	"included": true,
	"genus": "Aegilops"
}]
----

.Setting new taxonomic rules for the selected crop
include::{snippets}/crop-rules-update/curl-request.adoc[]


[cols="1,1,2", options="header"]
.Taxonomic rule fields
include::{snippets}/crop-rules-update/request-fields.adoc[]


=== Deleting a crop

A crop record can be deleted by issuing a HTTP DELETE request to the `/api/v0/crops/{shortName}`.
This will remove the crop and crop rules from the system.

.Deleting a crop
include::{snippets}/crop-delete/curl-request.adoc[]

