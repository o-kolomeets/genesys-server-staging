[[chApiRequests]]

== Managing Requests

Institutes can allow users to request for material through Genesys. Genesys will
validate the user's email address (by waiting for them to click the confirmation
link sent to their address) and also check if the user is registered with ITPGRFA's
Easy-SMTA database.

Genesys maintains a database of requests received, confirmed and dispatched to 
genebanks holding the requested materials. This API allows genebanks to retrieve
the request data from Genesys for automated integration into their existing information
systems.

[NOTE]
.Requests and sub-requests
=====================================================================
A client may request for material from several different genebanks in
one session. Genesys splits the request and dispatches sub-requests to
email addresses registered with individual genebanks. 
=====================================================================

Managing crop data in Genesys is done through methods available in */api/v0/requests*
namespace. 

=== Listing requests

To list all requests for the INSTCODE, you must have the `ADMINISTRATOR` permission
on the institute record. Issue a GET request to */api/v0/requests/{instCode}* endpoint:

include::{snippets}/requests-inst-list/curl-request.adoc[]

==== Path parameters

include::{snippets}/requests-inst-list/path-parameters.adoc[]

==== Request parameters

include::{snippets}/requests-inst-list/request-parameters.adoc[]

==== Server response

The object returned by Genesys contains pagination information and basic request data
in the `content` element:

[source,json,linenums]
----
{
	"content": [{
		"uuid": "4422e87e-52fe-38ef-b04a-079e663dd3da",
		"version": 0,
		"instCode": "XXX001",
		"instEmail": "institute@localhost",
		"state": 0,
		"createdDate": null,
		"lastModifiedDate": null,
		"lastReminderDate": null
	}], <1>
	"last": true,
	"totalElements": 1, <2>
	"totalPages": 1, <3>
	"size": 10, <4>
	"number": 0,
	"sort": [{
		"direction": "DESC",
		"property": "createdDate",
		"ignoreCase": false,
		"nullHandling": "NATIVE",
		"ascending": false
	}],
	"first": true,
	"numberOfElements": 1 <5>
}
----
<1> Array containing request information
<2> Number of elements in the Genesys database
<3> Number of pages
<4> Page size
<5> Number of elements in the `content` array

include::{snippets}/requests-inst-list/response-fields.adoc[]

=== Request details

To retrieve request details from Genesys:

include::{snippets}/requests-inst-details/curl-request.adoc[]

==== Path parameters

include::{snippets}/requests-inst-details/path-parameters.adoc[]

==== Server response

[source,json,linenums]
----
{
	"uuid": "7ecb3c6e-63d8-3869-b2c7-28571c9e2864",
	"version": 0,
	"instCode": "XXX001",
	"instEmail": "institute@localhost",
	"state": 0,
	"createdDate": null,
	"lastModifiedDate": null,
	"lastReminderDate": null
}
----

include::{snippets}/requests-inst-details/response-fields.adoc[]
