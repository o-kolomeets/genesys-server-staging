Accession passport data basics
==============================
Matija Obreza <matija.obreza@croptrust.org>
December 2015: Documentation commit {buildNumber}
:keywords: genebank documentation, accession passport data 
:revnumber: {projectVersion}
:doctype: book
:toc: left
:toclevels: 5
:icons: font
:numbered:
:source-highlighter: pygments
:pygments-css: class
:pygments-linenums-mode: table



[[intro]]
Introduction
------------

This manual contains basic information on commonly used standards for accession 
documentation and formats for data exchange. 

https://www.genesys-pgr.org[Genesys PGR] (Plant Genetic Resources) is a free online global portal accessible at
link:$$https://www.genesys-pgr.org$$[www.genesys-pgr.org]
that allows the exploration of the world’s crop diversity through a single website. The 
data published on Genesys follows the <<mcpd,Multi-crop Passport Descriptors>> standard.

The manual introduces

* <<wiews,FAO WIEWS>> database and <<wiews-instcode,WIEWS Institute codes>>
* FAO/Bioversity <<mcpd,Multi-crop Passport Descriptors>>
* <<mcpd-genesys,Genesys extensions>> to MCPD
* <<other-standards,Other standards>> relevant to accession documentation

== Acknowledgements

Special thanks go to Michael Mackay, Angela Marcela Hernandez, Edwin Rojas for their
input, feedback and support.

You can contact {author} at {email}.



include::sections/accedoc.adoc[]
include::sections/wiews.adoc[]
include::sections/mcpd.adoc[]
include::sections/iso.adoc[]

